using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlodSelf : MonoBehaviour
{
    public Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        _animator.SetBool("Hide",true);
        PlayerPrefs.SetInt("gold",PlayerPrefs.GetInt("gold")+1);
        Invoke("DelSelf",2);
    }
    void DelSelf()
    {
        Destroy(gameObject);
    }
}
