using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreController : MonoBehaviour
{
    private string _nowBuying;

    public int[] moneys;
    
    public GameObject buyPage;
    public Text much;

    public Text showGold;
    // Start is called before the first frame update
    void Start()
    {
        
        // PlayerPrefs.DeleteAll();
        if (!PlayerPrefs.HasKey("gold"))
        {
            PlayerPrefs.SetInt("gold",0);
            
        }
        showGold.text = PlayerPrefs.GetInt("gold") + "";
        
        if (!PlayerPrefs.HasKey("cloth1"))
        {
            PlayerPrefs.SetInt("cloth1",0);            
        }
        if (!PlayerPrefs.HasKey("cloth2"))
        {
            PlayerPrefs.SetInt("cloth2",0);            
        }
        
        transform.Find("购买衣服1").GetComponent<Button>().interactable=PlayerPrefs.GetInt("cloth1")==0;
        transform.Find("购买衣服2").GetComponent<Button>().interactable=PlayerPrefs.GetInt("cloth2")==0;
    }

    public void showBuyPage(string nowBuying)
    {
        PlayerPrefs.Save();
        if (nowBuying =="cloth1" || nowBuying == "cloth2")
        {
            if (PlayerPrefs.GetInt("gold") >= moneys[0])
            {
                PlayerPrefs.SetInt("gold",PlayerPrefs.GetInt("gold")-moneys[0]);
                PlayerPrefs.SetInt(nowBuying,1);
                transform.Find("购买衣服1").GetComponent<Button>().interactable=PlayerPrefs.GetInt("cloth1")==0;
                transform.Find("购买衣服2").GetComponent<Button>().interactable=PlayerPrefs.GetInt("cloth2")==0;
            }
            else
            {
                transform.Find("钱不够").gameObject.SetActive(true);
                Invoke("dismiss",2);
            }
        }
        else
        {
            _nowBuying = nowBuying;
            buyPage.SetActive(true);            
        }
        showGold.text = PlayerPrefs.GetInt("gold") + "";
    }

    public void add()
    {
        much.text = Convert.ToString(int.Parse(much.text) + 1);
    }

    public void sub()
    {
        much.text = Convert.ToString(int.Parse(much.text) - 1);
    }
    
    
    public void buy()
    {
        if (!PlayerPrefs.HasKey(_nowBuying))
        {
            PlayerPrefs.SetInt(_nowBuying,0);
        }
        
        int i =int.Parse(much.text);


        int money=999;
        if (_nowBuying=="ciTie")
        {
            money = moneys[2];
        }
        if (_nowBuying=="yinLiao")
        {
            money = moneys[3];
        }
        if (_nowBuying=="baoHu")
        {
            money = moneys[4];
        }
        
        if (PlayerPrefs.GetInt("gold")>= money*i)
        {
            PlayerPrefs.SetInt("gold",PlayerPrefs.GetInt("gold")-money*i);
            PlayerPrefs.SetInt(_nowBuying,PlayerPrefs.GetInt(_nowBuying)+i);
            buyPage.SetActive(false);
        }
        else
        {
            transform.Find("钱不够").gameObject.SetActive(true);
            Invoke("dismiss",2);
        }
        showGold.text = PlayerPrefs.GetInt("gold") + "";
    }

    void dismiss()
    {
        transform.Find("钱不够").gameObject.SetActive(false);
    }
    public void cancelBuy()
    {
        buyPage.SetActive(false);
    }

    public void cancelStore()
    {
        gameObject.SetActive(false);
    }
    
}
