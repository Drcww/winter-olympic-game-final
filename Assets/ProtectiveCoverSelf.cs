using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtectiveCoverSelf : MonoBehaviour
{
    public float time;

    public PropsControllor _propsControllor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        _propsControllor.OpenProtectiveCover(time);
        Destroy(gameObject);
    }
}
