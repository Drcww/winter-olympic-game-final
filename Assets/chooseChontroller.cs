using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class chooseChontroller : MonoBehaviour
{
    private int guanka;

    public Button[] Buttons;
    // Start is called before the first frame update
    void Start()
    {
        guanka = PlayerPrefs.GetInt("guanka");
        if (guanka>2)
        {
            guanka = 2;
        }
        for (int i = 0; i < guanka+1; i++)
        {
            Buttons[i].interactable = true;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void goDown()
    {
        SceneManager.LoadScene("Down");
    }
    public void go1()
    {
        SceneManager.LoadScene("1");
    }
    public void go2()
    {
        SceneManager.LoadScene("2");
    }
    public void go3()
    {
        SceneManager.LoadScene("3");
    }

    public void goMain()
    {
        SceneManager.LoadScene("StartScene");
    }
}
