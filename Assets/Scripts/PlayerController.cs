using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    public Rigidbody2D rb;
    public float speedUpCoefficient;
    public float slowDownCoefficient;
    public float rotateCoefficient;
    public Animator _animator;
    public LayerMask ground;
    public Transform centerOfMass;

    public bool samController;

    // public Transform jieChuDian;
    private BoxCollider2D _collider2D;
    private bool isGround;
    public float jumpForce;
    private int facedirection = -1;
    private int jumpCount = 2;
    private float time;
    public int score = 0;
    public float maxV;

    //人物状态
    private bool running;
    private bool slowing;
    private float rotate;

    void Start()
    {
        _collider2D = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //跳跃控制
        if (Input.GetButtonDown("Jump"))
        {
            time = Time.time;

            _animator.SetBool("JumpDown", true);
        }

        if (Input.GetButtonUp("Jump"))
        {
            _animator.SetBool("JumpDown", false);

            if (jumpCount > 0)
            {
                jumpCount--;
                float power = (Time.time - time) * 1000;
                power = power < 300 ? 300 : power;
                power = power > 600 ? 600 : power;
                rb.velocity = rb.transform.rotation * new Vector2(rb.velocity.x, power / 20 * jumpForce);
            }
        }

        //翻转控制
        if (Input.GetButtonDown("Roll"))
        {
            facedirection = -facedirection;
            transform.localScale = new Vector3(facedirection, 1, 1);
        }


        if (Input.GetButtonDown("FRoll"))
        {
            _animator.SetTrigger("FRoll");
            // rb.angularVelocity = 850;
        }


        //平衡动画
        _animator.SetFloat("Rotating", Input.GetAxisRaw("Rotate") * facedirection);
    }


    private void FixedUpdate()
    {
        //左右重合清零
        if (Input.GetAxisRaw("Horizontal") == 0)
        {
            running = false;
            slowing = false;
        }

        //地面移动控制
        GroundMovement();
        // SwitchAnim();

        _animator.SetBool("Running", running);
        _animator.SetBool("Slowing", slowing);


        //限速
        var velocity = rb.velocity;
        velocity = velocity.magnitude < maxV ? velocity : velocity.normalized * maxV;
        rb.velocity = velocity;
    }

    void GroundMovement()
    {
        float fastOrSlow = Input.GetAxisRaw("Horizontal");

        //空中逻辑
        rotate = -Input.GetAxisRaw("Rotate");
        if (rotate != 0)
        {
            rb.angularVelocity = rotate * rotateCoefficient;
            // rb.AddTorque(rotate*rotateCoefficient);
        }

        //地面逻辑
        if (samController)
        {
            if (fastOrSlow != 0 && isGround)
            {
                if (facedirection == 1) //朝左面
                {
                    if (fastOrSlow > 0) //如果加速
                    {
                        slowing = false;
                        running = true;
                        rb.AddForce(new Vector2(-fastOrSlow * speedUpCoefficient, 0));
                    }

                    if (fastOrSlow < 0) //减速键
                    {
                        slowing = true;
                        running = false;
                        if (rb.velocity.x < 0)
                        {
                            rb.AddForce(new Vector2(-fastOrSlow * slowDownCoefficient, 0));
                        }
                    }
                }
                else //朝右面
                {
                    if (fastOrSlow > 0) //如果加速
                    {
                        slowing = false;
                        running = true;
                        rb.AddForce(new Vector2(fastOrSlow * speedUpCoefficient, 0));
                    }

                    if (fastOrSlow < 0) //减速键
                    {
                        slowing = true;
                        running = false;
                        if (rb.velocity.x > 0)
                        {
                            rb.AddForce(new Vector2(fastOrSlow * slowDownCoefficient, 0));
                        }
                    }
                }
            }
        }
        else
        {
            if (fastOrSlow != 0 && isGround)
            {
                if (facedirection == 1) //朝左
                {
                    if (fastOrSlow < 0) //加速
                    {
                        slowing = false;
                        running = true;
                        rb.AddForce(new Vector2(fastOrSlow * speedUpCoefficient, 0));
                    }

                    if (fastOrSlow > 0) //如果按着d键
                    {
                        slowing = true;
                        running = false;
                        if (rb.velocity.x < 0) //有往左的速度
                        {
                            rb.AddForce(new Vector2(fastOrSlow * slowDownCoefficient, 0));
                        }
                    }
                }
                else //朝右
                {
                    if (fastOrSlow > 0) //加速
                    {
                        slowing = false;
                        running = true;
                        rb.AddForce(new Vector2(fastOrSlow * speedUpCoefficient, 0));
                    }

                    if (fastOrSlow < 0) //如果按着a键
                    {
                        slowing = true;
                        running = false;
                        if (rb.velocity.x > 0) //且有往右的速度
                        {
                            rb.AddForce(new Vector2(fastOrSlow * slowDownCoefficient, 0));
                        }
                    }
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            isGround = true;
            jumpCount = 1;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            isGround = false;
        }
    }
}