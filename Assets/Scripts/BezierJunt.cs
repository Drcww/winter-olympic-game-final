﻿using UnityEngine;

namespace DefaultNamespace
{
    public class BezierJunt
    {
        public Vector3 CalculateBezier3Point(float t, Vector3 p0, Vector3 p1, Vector3 p2,Vector3 p3)
        {
            return p0*Mathf.Pow(1-t,3)+p1 * (3.0f * t * Mathf.Pow(1-t,2))+p2 * (3.0f * Mathf.Pow(t,2) * (1-t))+p3*Mathf.Pow(t,3);
        }
        public float FindCalculateBezier3YByX(float x, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float jd)
        {
            if (x>p3.x)
            {
                return p3.y;
            }

            if (x<p0.x)
            {
                return p0.y;
            }
            float t0 = 0.0f;
            float t1 = 1.0f;

            Vector3 k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
        
            while (Mathf.Abs(k.x-x)>jd)
            {
                if (k.x>x)
                {
                    t1 =  (t0 + t1) / 2;
                }
                else
                {
                    t0 = (t0 + t1) / 2;
                }
                k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
            }
            return k.y;
        }
        public float xFindt(float x, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float jd)
        {
            if (x>p3.x)
            {
                return 1;
            }

            if (x<p0.x)
            {
                return 0;
            }
            float t0 = 0.0f;
            float t1 = 1.0f;

            Vector3 k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
        
            while (Mathf.Abs(k.x-x)>jd)
            {
                if (k.x>x)
                {
                    t1 =  (t0 + t1) / 2;
                }
                else
                {
                    t0 = (t0 + t1) / 2;
                }
                k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
            }
            return (t0 + t1) / 2;
        }
        public Vector3 findKBezier3(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            return Mathf.Pow((1 - t), 2) * (3 * (p1 - p0)) + 2 * t * (1 - t) * (3 * (p2 - p1)) +
                   Mathf.Pow(t, 2) * (3 * (p3 - p2));
        }
    }
}