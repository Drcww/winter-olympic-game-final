using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using Random = UnityEngine.Random;

public class Jisuan2 : MonoBehaviour
{
    public bool WHILE_SAFE_SWITCH = true;
    public GameObject object_p0, object_p1, object_p2,object_p3;
    public GameObject xPosition;
    public SpriteShapeController shapeController;
    private Spline spline;

    public int Fenduan;
    // Start is called before the first frame update
    void Start()
    {
        spline = shapeController.spline;
        for (int i = 0; i < spline.GetPointCount()-1; i++)
        {
            //四个点
            Vector3 p0 =spline.GetPosition(i);
            Vector3 p1 = p0+spline.GetRightTangent(i);
            Vector3 p3 =spline.GetPosition(i+1);
            Vector3 p2 = p3+spline.GetLeftTangent(i + 1);
            
            Debug.Log(p0+" "+ p1+" "+ p2+" "+ p3);
        }

    }

    // Update is called once per frame
    void Update()
    {
        // for (int i = 0; i < spline.GetPointCount()-1; i++)
        // {
        //     //四个点
        //     Vector3 p0 =spline.GetPosition(i);
        //     Vector3 p1 = p0+spline.GetRightTangent(i);
        //     Vector3 p3 =spline.GetPosition(i+1);
        //     Vector3 p2 = p3+spline.GetLeftTangent(i + 1);
        //     
        //     // 3阶贝塞尔绘制
        //     Vector3 lastPoint = p0;;
        //
        //     for (int ii = 0; ii <= Fenduan; ii++)
        //     {
        //         Vector3 cPoint = CalculateBezier3Point(1.0f / Fenduan * ii, p0, p1, p2, p3);
        //         // Debug.Log(lastPoint +  " " +  cPoint);
        //         Debug.DrawLine (lastPoint,cPoint ,Color.yellow);
        //         lastPoint = cPoint;
        //     }
        //     // Debug.Log(spline.GetPosition(i)+" "+spline.GetLeftTangent(i)+" "+spline.GetRightTangent(i));
        // }

        
        Vector3 p0, p1, p2,p3;
        p0 = object_p0.transform.position;
        p1 = object_p1.transform.position;
        p2 = object_p2.transform.position;
        p3 = object_p3.transform.position;
        Vector3 xPosition = this.xPosition.transform.position;
        
        // 3阶贝塞尔绘制
         Vector3 lastPoint = p0;
        
         for (int i = 0; i <= Fenduan; i++)
         {
             Vector3 cPoint = CalculateBezier3Point(1.0f / Fenduan * i, p0, p1, p2, p3);


             // Debug.Log(lastPoint +  " " +  cPoint);
             Debug.DrawLine (lastPoint,cPoint ,Color.yellow);
             lastPoint = cPoint;
         }

         float t = xFindt(xPosition.x, p0, p1, p2, p3, 0.2f);
         Vector3 cP = CalculateBezier3Point(t, p0, p1, p2, p3);
         Vector3 kPoint = findKBezier3(t, p0, p1, p2, p3);
         Debug.DrawLine(cP,cP+kPoint,Color.blue);
         
         // 寻找
         // Debug.Log(findCalculateBezier3YByX(xPosition.x,p0,p1,p2,p3,0.1f));
    }

    float xFindt(float x, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float jd)
    {
        if (x>p3.x)
        {
            return 1;
        }

        if (x<p0.x)
        {
            return 0;
        }
        float t0 = 0.0f;
        float t1 = 1.0f;

        Vector3 k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
        
        while (Mathf.Abs(k.x-x)>jd && WHILE_SAFE_SWITCH)
        {
            if (k.x>x)
            {
                t1 =  (t0 + t1) / 2;
            }
            else
            {
                t0 = (t0 + t1) / 2;
            }
            k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
        }
        return (t0 + t1) / 2;
    }
    
    float findCalculateBezier3YByX(float x, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float jd)
    {
        if (x>p3.x)
        {
            return p3.y;
        }

        if (x<p0.x)
        {
            return p0.y;
        }
        float t0 = 0.0f;
        float t1 = 1.0f;

        Vector3 k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
        
        while (Mathf.Abs(k.x-x)>jd && WHILE_SAFE_SWITCH)
        {
            if (k.x>x)
            {
                t1 =  (t0 + t1) / 2;
            }
            else
            {
                t0 = (t0 + t1) / 2;
            }
            k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
        }
        return k.y;
    }

    Vector3 findKBezier3(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        return Mathf.Pow((1 - t), 2) * (3 * (p1 - p0)) + 2 * t * (1 - t) * (3 * (p2 - p1)) +
               Mathf.Pow(t, 2) * (3 * (p3 - p2));
    }

    Vector3 CalculateBezier3Point(float t, Vector3 p0, Vector3 p1, Vector3 p2,Vector3 p3)
    {
        return p0*Mathf.Pow(1-t,3)+p1 * (3.0f * t * Mathf.Pow(1-t,2))+p2 * (3.0f * Mathf.Pow(t,2) * (1-t))+p3*Mathf.Pow(t,3);
    }
}
