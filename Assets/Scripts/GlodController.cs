using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlodController : MonoBehaviour
{
    public Transform player;
    public Animator Animator;
    public bool isMagnet;

    private float t;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (isMagnet)
        {
            float length = (transform.position - player.transform.position).magnitude;
            if (length <50f)
            {
                transform.position = Vector3.Lerp(transform.position, player.transform.position, 0.05f);
            }
            // if (t<1)
            // {
            //     transform.position = Vector3.Lerp(transform.position,player.transform.position,t);
            //     t += 1f * Time.deltaTime;
            // }
            // transform.position += (player.transform.position - transform.position) *10* Time.deltaTime;

        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        Animator.SetBool("Hide",true);
        Invoke("DestroyMyself",2f);
        
    }

    void DestroyMyself()
    {
        Destroy(transform.gameObject);
    }
    
}
