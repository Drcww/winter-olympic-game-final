using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jinbi : MonoBehaviour
{
    public PlayerController player;

    public Text Text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag=="Player")
        {
            player.score++;
            Text.text = "得分：" + player.score;
            GameObject.Destroy(gameObject);            
        }
    }
}
