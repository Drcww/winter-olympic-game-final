using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathController : MonoBehaviour
{
    public PlayableDirector overTimeLine;
    public Counter counter;
    public PropsControllorDown propsControllorDown;
    public PlayerControllerDown PlayerControllerDown;
    public Text ovetText;
    public bool sile;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (sile)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                SceneManager.LoadScene("Down");
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("Choose");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (propsControllorDown.isProtecting && propsControllorDown.isFlashing==false)
        {
            propsControllorDown.isFlashing = true;
            propsControllorDown.ProtectiveAnimator.SetInteger("ProtectiveState",2); //碰到地面就开始闪动
            propsControllorDown.CancelInvoke("CancelProtective");
            propsControllorDown.Invoke("CancelProtective",3);
        }
        else
        {
            if (!sile && !propsControllorDown.isProtecting)
            {
                sile = true;
                PlayerControllerDown.death = true;
                ovetText.text = "本次分数是 " + counter.Text.text+"\n按下空格重新游戏,ESC返回选关页面";
                overTimeLine.Play();
            }
        }

    }
}
