using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    private ParticleSystem ps;

    public Rigidbody2D rb;

    private ParticleSystem.EmissionModule em;

    private ParticleSystem.MainModule mn;
    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        em = ps.emission;
        mn = ps.main;
    }

    // Update is called once per frame
    void Update()
    {
        float v = rb.velocity.magnitude;
        v = v > 100 ? 100 : v;
        // em.rateOverDistance = v/3;
        mn.startSpeed = v/5;
    }
}
