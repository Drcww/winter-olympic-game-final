using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathController2 : MonoBehaviour
{
    public bool sile;
    public Text ovetText;
    public PropsControllor propsControllor;

    public PlayerController PlayerController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (sile)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("Choose");
            }
        }
        
    }
    private void OnCollisionEnter2D(Collision2D other)
    {      
        if (propsControllor.isProtecting)
        {
            propsControllor.isFlashing = true;
            propsControllor.ProtectiveAnimator.SetInteger("ProtectiveState",2); //碰到地面就开始闪动
            propsControllor.CancelInvoke("CancelProtective");
            propsControllor.Invoke("CancelProtective",3);
        }
        else
        {
            if (!sile)
            {
                sile = true;
                ovetText.text = "游戏结束\n按下空格重新游戏,按esc返回选关页面";
            }
        }


    }
}
