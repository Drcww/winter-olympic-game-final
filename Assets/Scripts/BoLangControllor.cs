using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoLangControllor : MonoBehaviour
{

    private bool isStart;
    private float _timeLength; 
    public float percent;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (isStart)
        {
            if (percent >0)
            {
                percent = percent - Time.deltaTime/(1.0f*_timeLength);
            }
            else
            {
                percent = 0f;
                isStart = false;
            }
        }
        else
        {
            if (percent < 1) 
            {
                percent = percent + Time.deltaTime/10.0f;
            }
            else
            {
                percent = 1f;
            }
        }
        transform.localPosition = new Vector3(transform.localPosition.x, -78 + 73 / 100.0f*percent*100, 0);
        // Debug.Log(percent);
    }
    public void StartProp(float timeLength)
    {
        isStart = true;
        _timeLength = timeLength;
    }
}
