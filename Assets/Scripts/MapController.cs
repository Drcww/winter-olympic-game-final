using UnityEngine;
using UnityEngine.U2D;

public class MapController : MonoBehaviour
{
    public SpriteShapeController shapeControllerPreset;
    public SpriteShapeController shapeController2Preset;
    public SpriteShapeController shapeControllerDjPreset;
    public SpriteShapeController shapeControllerDj2Preset;

    private SpriteShapeController _shapeControllerNew;
    private SpriteShapeController _shapeControllerNew2;
    private SpriteShapeController _shapeControllerOld;
    private SpriteShapeController _shapeControllerOld2;

    public Transform player;
    public GameObject treeB;
    public GameObject treeF;
    public GameObject muzhuang;
    public GameObject Glod;
    private Vector3 _nowLastPoint;

    private int _nowPointMode;

    private Spline _spline;
    private Spline _spline2;
    private int _count;

    private bool _go2;

    // Start is called before the first frame update
    void Start()
    {
        _shapeControllerNew = shapeControllerPreset;
        _shapeControllerNew2 = shapeController2Preset;
        _spline = _shapeControllerNew.spline;
        _spline2 = _shapeControllerNew2.spline;
        _count = _spline.GetPointCount();
        _nowLastPoint = _spline.GetPosition(_count - 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (_go2) //如果正在前往第二段地形
        {
            Spline oldSpline = _shapeControllerOld.spline; //获取旧地形spline


            Vector3 dbg = oldSpline.GetPosition(oldSpline.GetPointCount() - 1) +
                          _shapeControllerOld.transform.position; //debug用的
            Debug.DrawLine(dbg, dbg + new Vector3(200, 0, 0)); //debug用的

            if (player.position.x - oldSpline.GetPosition(oldSpline.GetPointCount() - 1).x -
                _shapeControllerOld.transform.position.x > 200) //如果玩家超出旧地形最后一点控制点300单位
            {
                _go2 = false; //说明玩家已处于新地形 可以取消正在前往
                GameObject.Destroy(_shapeControllerOld.gameObject); //删除旧地形
                GameObject.Destroy(_shapeControllerOld2.gameObject);
            }
        }

        //Debug刷新线
        Debug.DrawLine(_nowLastPoint + _shapeControllerNew.transform.position + new Vector3(-300, 0, 0),
            _nowLastPoint + _shapeControllerNew.transform.position + new Vector3(-300, 200, 0), Color.magenta);
        //Debug刷新线
        if (_nowLastPoint.x + _shapeControllerNew.transform.position.x - player.position.x < 300 &&
            _go2 == false) //如果玩家距离当前地形最后一控制点小于150单位
        {
            StepMain(); //刷新新地形
            DelBeyond(); //清除旧数据
        }
    }

    void StepMain()
    {
        if (_go2) //如果还没到新地形就不要刷新新地形
        {
            _nowPointMode = Random.Range(1, 5);
        }
        else
        {
            _nowPointMode = Random.Range(1, 6);
        }


        // Debug.Log("sc:"+_nowPointMode);

        if (_nowPointMode == 1) //直弯
        {
            Vector3 offset = new Vector3(Random.Range(100, 200), Random.Range(-50, -100), 0);
            int j = Random.Range(20, 50);
            AddStep(offset, new Vector3(-j, 0, 0), new Vector3(j, 0, 0));
            BuildObject(_nowPointMode, 0, _spline); //生成装饰
        }

        if (_nowPointMode == 2) //上弯
        {
            Vector3 offset = new Vector3(Random.Range(100, 200), Random.Range(-50, -100), 0);
            int j = Random.Range(20, 50);
            int k = Random.Range(20, 40);
            AddStep(offset, new Vector3(-j, -k, 0), new Vector3(j, k, 0));
            BuildObject(_nowPointMode, 0, _spline); //生成装饰
        }

        if (_nowPointMode == 3) //下弯
        {
            Vector3 offset = new Vector3(Random.Range(100, 200), Random.Range(-50, -100), 0);
            int j = Random.Range(20, 50);
            int k = Random.Range(20, 40);
            AddStep(offset, new Vector3(-j, k, 0), new Vector3(j, -k, 0));
            BuildObject(_nowPointMode, 0, _spline); //生成装饰
        }

        if (_nowPointMode == 4) //小土坡
        {
            int random = Random.Range(10, 30);
            Vector3 offset = new Vector3(100, 0, 0);
            AddStep(offset, new Vector3(-random / 2f, 0, 0), new Vector3(random / 2f, 0, 0)); //第一个点

            offset = new Vector3(random * 1.5f, random, 0);
            AddStep(offset, new Vector3(-random / 2f, 0, 0), new Vector3(random / 2f, 0, 0)); //2

            offset = new Vector3(random * 2f, 0, 0);
            AddStep(offset, new Vector3(-random / 2f, 0, 0), new Vector3(random / 2f, 0, 0)); //3

            offset = new Vector3(random * 3f, -random * 2, 0);
            AddStep(offset, new Vector3(-random / 2f, 0, 0), new Vector3(random / 2f, 0, 0)); //4
            BuildObject(_nowPointMode, 2, _spline); //生成装饰
        }

        if (_nowPointMode == 5) //分离地形生成
        {
            int random = Random.Range(70, 100); //随机大小
            Vector3 offset = new Vector3(random * 1.5f, -random, 0);

            AddStep(offset, new Vector3(-random / 2f, 0, 0), new Vector3(random / 2f, 0, 0)); //先生成一个下面的直弯
            BuildObject(_nowPointMode, 0, _spline); //生成装饰 

            offset = new Vector3(random * 1.2f, random * 0.5f);
            AddStep(offset, new Vector3(0, random / 50f, 0), new Vector3(0, -random / 50f, 0)); //再上抬头
            BuildObject(_nowPointMode, 1, _spline); //生成装饰  
            // BuildObject(_spline);//生成装饰
            offset = new Vector3(0, -random * 1f);
            AddStep(offset, new Vector3(0, random / 50f, 0), new Vector3(0, -random / 50f, 0)); //下面收尾的
            BuildObject(_nowPointMode, 2, _spline); //生成装饰  

            //新地形变旧地形
            _shapeControllerOld = _shapeControllerNew;
            _shapeControllerOld2 = _shapeControllerNew2;

            //生成新地形
            Vector3 t = _shapeControllerNew.transform.position + _spline.GetPosition(_count - 1); //生成位置是旧地形最后一点控制点的位置
            t.x = t.x + 75; //新地形偏移
            t.y = t.y - 20;
            _shapeControllerNew = Instantiate(shapeControllerDjPreset, t, Quaternion.identity); //生成新地形
            t.y = t.y - 6;
            _shapeControllerNew2 = Instantiate(shapeControllerDj2Preset, t, Quaternion.identity);

            _spline = _shapeControllerNew.spline; //修改现在的spline为新的
            _spline2 = _shapeControllerNew2.spline;

            _go2 = true; //设置玩家为正在向新地形移动

            _count = _spline.GetPointCount(); //重置控制点计数
            _nowLastPoint = _spline.GetPosition(_count - 1); //重置末尾控制点位置
        }
    }

    void AddStep(Vector3 offset, Vector3 left, Vector3 right)
    {
        _count = _spline.GetPointCount();
        Vector3 p = _nowLastPoint + offset;
        //插入新点
        _spline.InsertPointAt(_count, p);
        _spline.SetTangentMode(_count, ShapeTangentMode.Continuous);
        _spline.SetLeftTangent(_count, left);
        _spline.SetRightTangent(_count, right);
        //s2
        _spline2.InsertPointAt(_count, p);
        _spline2.SetTangentMode(_count, ShapeTangentMode.Continuous);
        _spline2.SetLeftTangent(_count, left);
        _spline2.SetRightTangent(_count, right);

        _nowLastPoint = p; //让现在的最后一个控制点等于新控制点

        //移动0点
        _spline.SetPosition(0, new Vector3(_spline.GetPosition(1).x, _nowLastPoint.y, 0));
        _shapeControllerNew.BakeCollider();

        _spline2.SetPosition(0, new Vector3(_spline2.GetPosition(1).x, _nowLastPoint.y, 0));
        _shapeControllerNew2.BakeCollider();
    }

    void MakeObject(GameObject obj, float x, float y, Vector3 s)
    {
        GameObject gameObject = Instantiate(obj, new Vector3(x, y, 0), Quaternion.identity);
        gameObject.transform.localScale = s;
    }

    void DelBeyond()
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Daoju");
        foreach (var variable in gameObjects)
        {
            if ((variable.transform.position - player.transform.position).magnitude > 500)
            {
                GameObject.Destroy(variable);
            }
        }

        if ((player.position - _spline.GetPosition(1) - _shapeControllerNew.transform.position).x > 300 &&
            _go2 == false) //如果玩家距离地形第一个控制点位置大于300
        {
            _spline.RemovePointAt(1); //删除第1点
            _spline2.RemovePointAt(1); //删除第1点
        }
    }

    Vector3 CalculateBezier3Point(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        return p0 * Mathf.Pow(1 - t, 3) + p1 * (3.0f * t * Mathf.Pow(1 - t, 2)) +
               p2 * (3.0f * Mathf.Pow(t, 2) * (1 - t)) + p3 * Mathf.Pow(t, 3);
    }

    float FindCalculateBezier3YByX(float x, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float jd)
    {
        if (x > p3.x)
        {
            return p3.y;
        }

        if (x < p0.x)
        {
            return p0.y;
        }

        float t0 = 0.0f;
        float t1 = 1.0f;

        Vector3 k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);

        while (Mathf.Abs(k.x - x) > jd)
        {
            if (k.x > x)
            {
                t1 = (t0 + t1) / 2;
            }
            else
            {
                t0 = (t0 + t1) / 2;
            }

            k = CalculateBezier3Point((t0 + t1) / 2, p0, p1, p2, p3);
        }

        return k.y;
    }

    void BuildObject(int type, int index, Spline spline)
    {
        Debug.Log(type + " " + index);
        if (type >= 1 && type <= 3)
        {
            Vector3 p0, p1, p2, p3;
            p0 = spline.GetPosition(_count - 2);
            p1 = p0 + spline.GetRightTangent(_count - 2);
            p3 = spline.GetPosition(_count - 1);
            p2 = p3 + spline.GetLeftTangent(_count - 1);

            int size = Random.Range(2, 1);
            float spacing = (p3 - p0).x / size; //间距

            for (int i = 0; i < size; i++)
            {
                float x = p0.x + spacing * i; //插
                float y = FindCalculateBezier3YByX(x, p0, p1, p2, p3, 1); //在这个x位置上获取他的y
                // MakeObject(Random.Range(0,2)==0?treeF:treeB,x,y+20);//是0就生成前面的树不然就后面
                MakeObject(treeB, _shapeControllerNew.transform.position.x + x,
                    _shapeControllerNew.transform.position.y + y + 20,
                    new Vector3(1, Random.Range(0.8f, 1.5f), 1)); //是0就生成前面的树不然就后面
            }

            //金币
            size = Random.Range(3, 5);
            float offset = (p3 - p0).x / 4;
            spacing = offset * 2 / size;
            for (int i = 0; i < size; i++)
            {
                float x = p0.x + offset + spacing * i; //插
                float y = FindCalculateBezier3YByX(x, p0, p1, p2, p3, 1); //在这个x位置上获取他的y
                // MakeObject(Random.Range(0,2)==0?treeF:treeB,x,y+20);//是0就生成前面的树不然就后面
                MakeObject(Glod, _shapeControllerNew.transform.position.x + x,
                    _shapeControllerNew.transform.position.y + y + 8, Vector3.one); //是0就生成前面的树不然就后面
            }
        }

        else if (type == 4)
        {
            if (index == 2)
            {
                Vector3 p0, p1, p2, p3;
                p0 = spline.GetPosition(_count - 2);
                p1 = p0 + spline.GetRightTangent(_count - 2);
                p3 = spline.GetPosition(_count - 1);
                p2 = p3 + spline.GetLeftTangent(_count - 1);

                int size = Random.Range(1, 2);
                float spacing = (p3 - p0).x / size; //间距

                for (int i = 0; i < size; i++)
                {
                    float x = p0.x + spacing * i; //插
                    float y = FindCalculateBezier3YByX(x, p0, p1, p2, p3, 1); //在这个x位置上获取他的y
                    // MakeObject(Random.Range(0,2)==0?treeF:treeB,x,y+20);//是0就生成前面的树不然就后面
                    MakeObject(treeB, _shapeControllerNew.transform.position.x + x,
                        _shapeControllerNew.transform.position.y + y + 20,
                        new Vector3(1, Random.Range(0.8f, 1.5f), 1)); //是0就生成前面的树不然就后面
                }
            }
        }
        else if (type == 5)
        {
            if (index == 0 || index == 2)
            {
                Vector3 p0, p1, p2, p3;
                p0 = spline.GetPosition(_count - 2);
                p1 = p0 + spline.GetRightTangent(_count - 2);
                p3 = spline.GetPosition(_count - 1);
                p2 = p3 + spline.GetLeftTangent(_count - 1);

                int size = Random.Range(1, 1);
                float spacing = (p3 - p0).x / size; //间距

                for (int i = 0; i < size; i++)
                {
                    float x = p0.x + spacing * i; //插
                    float y = FindCalculateBezier3YByX(x, p0, p1, p2, p3, 1); //在这个x位置上获取他的y
                    // MakeObject(Random.Range(0,2)==0?treeF:treeB,x,y+20);//是0就生成前面的树不然就后面
                    MakeObject(treeB, _shapeControllerNew.transform.position.x + x,
                        _shapeControllerNew.transform.position.y + y + 20,
                        new Vector3(1, Random.Range(0.8f, 1.5f), 1)); //是0就生成前面的树不然就后面
                }
            }

            if (index == 1)
            {
                Vector3 p0, p1, p2, p3;
                p0 = spline.GetPosition(_count - 2);
                p1 = p0 + spline.GetRightTangent(_count - 2);
                p3 = spline.GetPosition(_count - 1);
                p2 = p3 + spline.GetLeftTangent(_count - 1);

                int size = Random.Range(4, 2);
                float spacing = (p3 - p0).x / size; //间距

                for (int i = 0; i < size; i++)
                {
                    float x = p0.x + spacing * i; //插
                    float y = FindCalculateBezier3YByX(x, p0, p1, p2, p3, 1); //在这个x位置上获取他的y
                    // MakeObject(Random.Range(0,2)==0?treeF:treeB,x,y+20);//是0就生成前面的树不然就后面
                    MakeObject(treeB, _shapeControllerNew.transform.position.x + x,
                        _shapeControllerNew.transform.position.y + y + 20,
                        new Vector3(1, Random.Range(0.8f, 1.5f), 1)); //是0就生成前面的树不然就后面
                }
            }
        }

        //新生成办法（贝塞尔计算）
        // Debug.Log("1111");
        //树


        //
        //
        // for (int i = 0; i < Random.Range(0,5); i++)
        // {
        //     int x = (int)(_shapeControllerNew.transform.position.x+Random.Range(p3.x, p0.x)); //取倒数第二个点和倒数第一个点中x的一个随机数
        //     int y = (int)(_shapeControllerNew.transform.position.y+FindCalculateBezier3YByX(x, p0,p1,p2,p3,1)); //在这个x位置上获取他的y
        //     MakeObject(Random.Range(0,2)==0?treeF:treeB,x,y+20);//是0就生成前面的树不然就后面
        // }
        //木桩
        // for (int i = 0; i < Random.Range(0,3); i++)
        // {
        //     int x = (int)(_shapeControllerNew.transform.position.x+Random.Range(p3.x, p0.x)); //取倒数第二个点和倒数第一个点中x的一个随机数
        //     int y = (int)(_shapeControllerNew.transform.position.y+FindCalculateBezier3YByX(x, p0,p1,p2,p3,1)); //在这个x位置上获取他的y
        //     MakeObject(muzhuang,x,y+7);//是0就生成前面的树不然就后面
        // }
    }
}