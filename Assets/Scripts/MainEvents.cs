using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainEvents : MonoBehaviour
{
    public Image aboutP;

    public GameObject Store;

    public Slider Slider;

    public AudioSource AudioSource;

    public GameObject yinliang;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (aboutP.enabled)
        {
            if (Input.GetMouseButton(0))
            {
                aboutP.enabled=false; 
            }
        }

        if (Input.GetKey(KeyCode.D)&&Input.GetKey(KeyCode.R)&&Input.GetKey(KeyCode.C)&&Input.GetKey(KeyCode.W))
        {
            
           PlayerPrefs.SetInt("gold",9999); 
        }
    }

    public void OnClickPlayBtn()
    {
        SceneManager.LoadScene("Choose");
    }

    public void OnClickExitBtn()
    {
        Application.Quit();
    }

    public void OnClickStoreBtn()
    {
        Store.SetActive(true);
    }

    public void setyinliang()
    {
        AudioSource.volume = Slider.value;
    }

    public void showYinliang()
    {
        yinliang.SetActive(true);
    }

    public void hideYinliang()
    {
        yinliang.SetActive(false);
    }
}
