using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneController : MonoBehaviour
{
    // public SceneAsset ChooseScene;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GotoChoose()
    {
        SceneManager.LoadSceneAsync("Choose");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
