using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PlayerControllerDown : MonoBehaviour
{
 // Start is called before the first frame update

    public Rigidbody2D rb;
    public float speedUpCoefficient;
    public float rotateCoefficient;
    public Animator _animator;
    private BoxCollider2D _collider2D;
    private bool isGround;
    public float jumpForce;
    private int facedirection = -1;
    private int jumpCount=2;
    private long time;
    private bool start=false;
    public ParticleSystem ps;
    public TrailRenderer tr;

    //人物状态
    private bool running;
    private bool slowing;
    // private bool jumping;
    // private bool falling;
    private float rotate;

    public bool death=false;
    public int SkateboardType;

    private float x;

    void Start()
    {
        _collider2D = GetComponent<BoxCollider2D>();
        x = transform.position.x;

    }

    // Update is called once per frame
    void Update()
    {        
        //跳跃控制
        if (Input.GetButtonDown("Jump"))
        {
            time = System.Environment.TickCount;
            
            _animator.SetBool("JumpDown",true);
        }

        if (Input.GetButtonUp("Jump"))
        {
            _animator.SetBool("JumpDown",false);

            if (isGround)
            {
                int power =(int) (System.Environment.TickCount - time);
                power = power > 500 ? 500 : power;
                rb.velocity = rb.transform.rotation * new Vector2(rb.velocity.x, power / 20 * jumpForce);
            }
        }


        //粒子\拖尾控制
        if (isGround)
        {
            ps.Play();
            tr.emitting = false;
        }
        else
        {
            ps.Stop();
            tr.emitting = true;
        }



        
        //平衡动画
        _animator.SetFloat("Rotating",Input.GetAxisRaw("Rotate")*facedirection);

        // if (Input.GetButtonDown("Horizontal"))
        // {
        //     _animator.SetBool("Running",true);
        //     
        // }
        // if (Input.GetButtonUp("Horizontal"))
        // {
        //     _animator.SetBool("Running",false);
        // }

        // Jump();
    }

    void Jump()
    {
        if ( Input.GetButtonDown("Jump"))
        {
            if (isGround)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                jumpCount--;
            }
            else if (jumpCount > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                jumpCount--;
            }
        }

    }

    public void useEngeryWater(bool enable)
    {
        var v = ps.velocityOverLifetime;
        v.enabled = enable;

        var c = ps.colorOverLifetime;
        Gradient gradient = new Gradient();
        Color color = enable ? new Color(255,100,0) : new Color(196, 221, 216);
        gradient.SetKeys(
            new GradientColorKey[]{new GradientColorKey(color,0.0f)},
            new GradientAlphaKey[]{new GradientAlphaKey(1.0f,0.0f),new GradientAlphaKey(0.0f,0.16f)}
            );
        c.color = gradient;
    }
    
    private void FixedUpdate()
    {

        if (!death)
        {

            if (SkateboardType==0)
            {
                //常驻前进
                if (start)
                {
                    rb.velocity = new Vector2(speedUpCoefficient, rb.velocity.y);            
                }
                //地面移动控制
                GroundMovement();
                SwitchAnim();
            }
            else
            {
                x += 1;
            }
            

        



            _animator.SetBool("Running",running);
            _animator.SetBool("Slowing",slowing);
        }
        
    }

    void GroundMovement()
    {
        // float horizontalmove = Input.GetAxis("Horizontal");

        //手柄优化
        // if (fastOrSlow!=0)
        // {
        //     fastOrSlow = fastOrSlow > 0 ? 1 : -1;            
        // }
        // _animator.SetFloat("running",Mathf.Abs(fastOrSlow));

        float fastOrSlow = Input.GetAxisRaw("Horizontal");
        
        //空中逻辑
        rotate = -Input.GetAxisRaw("Rotate");
        if (rotate!=0 && !isGround)
        {
            rb.angularVelocity = rotate * rotateCoefficient;
            // rb.AddTorque(rotate*rotateCoefficient);
        }
    }

    void SwitchAnim()
    {
        // if (isGround)
        // {
        //     jumping = false;
        //     falling = false;
        //     // _animator.SetBool("falling",false);
        //     // _animator.SetBool("Jumping",false);
        // }
        // else if (rb.velocity.y>0)
        // {
        //     jumping = true;
        //     // _animator.SetBool("Jumping",true);
        // }
        // else if (rb.velocity.y < 0)
        // {
        //     falling = true;
        //     // _animator.SetBool("Jumping",false);
        //     // _animator.SetBool("falling",true);
        // }
    }
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.layer==LayerMask.NameToLayer("Ground"))
        {
            isGround = true;
            jumpCount = 2;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.layer==LayerMask.NameToLayer("Ground"))
        {
            isGround = false;
        }
    }

    public void StartRun()
    {
        start = true;
        running = true;
    }
}
