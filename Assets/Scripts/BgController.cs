using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgController : MonoBehaviour
{

    public Transform player;
    public Transform[] mountains;
    public float[] V_mountains;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // moveMountainous(m1,Player,V_m1);
        // moveMountainous( m2,Player,V_m2);
        // moveMountainous(m3,Player,V_m3);
        
        // moveMountainous(c1,Player,V_c1);
        // moveMountainous( c2,Player,V_c2);
        // moveMountainous(c3,Player,V_c3);
        transform.position =player.position;
        for (int i = 0; i < mountains.Length; i++)
        {
            mountains[i].localPosition=new Vector3(player.position.x / -100 * V_mountains[i], mountains[i].localPosition.y,0);
        }
    }

    // private void moveMountainous(Transform mountain, Transform player,float v)
    // {
    //     Vector3 position = mountain.localPosition;
    //     mountain.localPosition=new Vector3(player.position.x / -100 * v, position.y,0);
    // }    
    private void moveMountainous(Transform mountain, Transform player,float v)
    {
        mountain.localPosition=new Vector3(player.position.x / -100 * v, 0,0);
    }

}
