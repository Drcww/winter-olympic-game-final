using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PropsControllor : MonoBehaviour
{
    public BoLangControllor Magnet;
    public bool isMagnet;
    public Animator MagnetAnimator;
    public BoLangControllor ProtectiveCover;
    public Animator ProtectiveAnimator;
    public BoLangControllor EnergyWater;
    public Animator EngeryWaterAnimator;
    
    public bool isProtecting = false;
    public bool isFlashing;

    public PlayerController playerController;

    public Text text_EngeryWater;
    public Text text_Protective;
    public Text text_Magnet;

    public Text text_Glod;

    private int passGold;
    // Start is called before the first frame update
    void Start()
    {

        passGold = PlayerPrefs.GetInt("gold");
    }

    // Update is called once per frame
    void Update()
    {
        text_EngeryWater.text = "x"+PlayerPrefs.GetInt("yinLiao");
        text_Protective.text = "x"+PlayerPrefs.GetInt("baoHu");
        text_Magnet.text = "x"+PlayerPrefs.GetInt("ciTie");
        text_Glod.text = PlayerPrefs.GetInt("gold")-passGold+"";
        if (Input.GetKeyDown(KeyCode.Alpha1) && Magnet.percent>=1 && PlayerPrefs.GetInt("ciTie")>0)
        {
            PlayerPrefs.SetInt("ciTie",PlayerPrefs.GetInt("ciTie")-1);
            text_Magnet.text = "x"+PlayerPrefs.GetInt("ciTie");
            
            isMagnet = true;
            Magnet.StartProp(30);
            MagnetAnimator.SetInteger("MagnetState",1);
            Invoke("CancelMagnet",30);
        }

        if (isMagnet)
        {
            GameObject[] glods = GameObject.FindGameObjectsWithTag("Glod");
            foreach (var v in glods)
            {
                float length = (v.transform.position - transform.position).magnitude;
                if (length <50f)
                {
                    v.transform.position = Vector3.Lerp(v.transform.position, transform.position, 0.05f);
                }
            }
        }
        
        
        if (Input.GetKeyDown(KeyCode.Alpha2) && ProtectiveCover.percent>=1 && PlayerPrefs.GetInt("baoHu")>0)
        {
            PlayerPrefs.SetInt("baoHu",PlayerPrefs.GetInt("baoHu")-1);
            text_Protective.text = "x"+PlayerPrefs.GetInt("baoHu");
            
            ProtectiveAnimator.SetInteger("ProtectiveState",1);
            isProtecting = true;
            ProtectiveCover.StartProp(10);
            Invoke("CancelProtective",10);
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha3) && EnergyWater.percent>=1 && PlayerPrefs.GetInt("yinLiao")>0)
        {
            PlayerPrefs.SetInt("yinLiao",PlayerPrefs.GetInt("yinLiao")-1);
            text_EngeryWater.text = "x"+PlayerPrefs.GetInt("yinLiao");
            
            EngeryWaterAnimator.SetInteger("EngeryWaterState",1);
            playerController.speedUpCoefficient = playerController.speedUpCoefficient * 1.5f;
            // PlayerControllerDown.useEngeryWater(true);
            EnergyWater.StartProp(10);
            Invoke("CancleSpeed",10);
        }
        
        
    }


    public void OpenMagnet(float time)
    {
        isMagnet = true;
        MagnetAnimator.SetInteger("MagnetState",1);
        Invoke("CancelMagnet",time);
    }

    public void OpenProtectiveCover(float time)
    {
        ProtectiveAnimator.SetInteger("ProtectiveState",1);
        isProtecting = true;
        Invoke("CancelProtective",time);
    }

    public void OpenEngeryWater(float time,float speedX)
    {
        EngeryWaterAnimator.SetInteger("EngeryWaterState",1);
        playerController.speedUpCoefficient = playerController.speedUpCoefficient * speedX;
        // PlayerControllerDown.useEngeryWater(true);
        Invoke("CancleSpeed",time);
    }
    
    
    void CancelMagnet()
    {
        isMagnet = false;
        MagnetAnimator.SetInteger("MagnetState",0);
    }
    
    void CancelProtective()
    {
        ProtectiveAnimator.SetInteger("ProtectiveState",0);
        isFlashing = true;
        isProtecting = false;
    }
    
    void CancleSpeed()
    {
        EngeryWaterAnimator.SetInteger("EngeryWaterState",0);
        playerController.speedUpCoefficient = playerController.speedUpCoefficient / 1.5f;
        // playerController.useEngeryWater(false);
    }
    
}
