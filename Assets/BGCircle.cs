using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGCircle : MonoBehaviour
{
    public Transform p1, p2;
    public float BianJu;
    public float offset;
    private Transform pt;
    private Vector3 p1E, p2E,ptE;
    public Transform player;
    // Start is called before the first frame update
    void Start()
    {
        p1E = p1.GetComponent<SpriteRenderer>().sprite.bounds.extents;
        p2E = p2.GetComponent<SpriteRenderer>().sprite.bounds.extents;
    }

    // Update is called once per frame
    void Update()
    {
        // if (p2.position.x+p2E.x-BianJu -player.position.x <player.position.x-p1.position.x-p1E.x+BianJu)
        // {
        //     
        // }
        
        if (player.position.x>p2.position.x+p2E.x-BianJu)
        {
            Vector3 t = p1.position;
            p1.position = new Vector3(p2.position.x + p2E.x * 2 - offset, t.y, t.z);
            //交换
            pt = p1;
            p1 = p2;
            p2 = pt;

            ptE = p1E;
            p1E = p2E;
            p2E = ptE;
        }
        
        if (player.position.x<p1.position.x-p1E.x+BianJu)
        {
            Vector3 t = p2.position;
            p2.position = new Vector3(p1.position.x - p1E.x * 2 + offset, t.y, t.z);
            //交换
            pt = p1;
            p1 = p2;
            p2 = pt;
            ptE = p1E;
            p1E = p2E;
            p2E = ptE;
        }
    }
}
